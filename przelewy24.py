from flask import Flask, request
import hashlib
import urllib.parse
from random import randint
import threading
import time
import requests
app = Flask(__name__)

def md5(s):
    return hashlib.md5(s.encode('utf-8')).hexdigest()

def p24_md5(*a):
    return md5('|'.join([str(b) for b in a]))

def response(data):
    a = []
    for key, value in data:
        k = urllib.parse.quote_plus(key)
        v = urllib.parse.quote_plus(value)
        a.append(k+'='+v)
    return '&'.join(a)

def error(code='err01'):
    return response([
        ('error', code),
        ('errorMessage', 'something'),
    ])

p24_merchant_id = '7357'
p24_pos_id = p24_merchant_id
salt = 'salt'


def basic_validate():
    if(request.form['p24_merchant_id'] != p24_merchant_id):
        return error('1')
    if(request.form['p24_pos_id'] != p24_pos_id):
        return error('2')


def status(p24_url_status, data):
    print('<', p24_url_status, '>')
    time.sleep(2)
    r = requests.post(p24_url_status, data=data)
    with open('problem2.html', 'w') as file:
        file.write(r.text)
    print('</', p24_url_status, '>')


@app.route("/testConnection", methods=['POST'])
def hello():
    r = basic_validate()
    if r:
        return r
    if(request.form['p24_sign'] != p24_md5(p24_pos_id, salt)):
        return error('3')
    return response([
        ('error', '0'),
    ])


@app.route("/trnRegister", methods=['POST'])
def trnRegister():
    r = basic_validate()
    if r:
        return r

    p24_url_status = request.form['p24_url_status']

    p24_session_id = request.form['p24_session_id']
    p24_order_id = randint(0, 1000)
    p24_amount = request.form['p24_amount']
    p24_currency =request.form['p24_currency']

    data = {
        'p24_merchant_id': request.form['p24_merchant_id'],
        'p24_pos_id': request.form['p24_pos_id'],
        'p24_session_id': request.form['p24_session_id'],
        'p24_amount': request.form['p24_amount'],
        'p24_currency': request.form['p24_currency'],
        'p24_order_id': p24_order_id,
        'p24_method': 'paycos',
        'p24_statement': 'Nazwa przelewu',
        'p24_sign': p24_md5(
            p24_session_id,
            p24_order_id,
            p24_amount,
            p24_currency,
            salt,
        ),
    }

    t1 = threading.Thread(target = status, args = (
        p24_url_status, data
    ))
    t1.start()

    return response([
        ('error', '0'),
        ('token', 'abb123'),
    ])


@app.route("/trnVerify", methods=['POST'])
def trnVerify():
    r = basic_validate()

    return response([
        ('error', '0'),
    ])


# example md5 test
print(md5('abcdefghijk|9999|2500|PLN|a123b456c789d012'))
print(p24_md5('abcdefghijk','9999','2500','PLN','a123b456c789d012'))
print('6c7f0bb62c046fbc89921dc3b2b23ede')
